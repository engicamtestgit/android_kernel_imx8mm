/*
 * Copyright 2018 NXP
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/dts-v1/;

#include "../freescale/fsl-imx8mm.dtsi"

/ {
	firmware {
		android {
			compatible = "android,firmware";
			fstab {
				compatible = "android,fstab";
				vendor {
					compatible = "android,vendor";
					/* sd card node which used if androidboot.storage_type=sd */
					dev_sd = "/dev/block/platform/30b40000.mmc/by-name/vendor";
					/* emmc node which used if androidboot.storage_type=emmc */
					dev_emmc = "/dev/block/platform/30b60000.mmc/by-name/vendor";
					/* default emmc node used for GSI */
					dev = "/dev/block/platform/30b60000.mmc/by-name/vendor";
					type = "ext4";
					mnt_flags = "ro,barrier=1,inode_readahead_blks=8";
					fsmgr_flags = "wait,slotselect,avb";
				};
			};
			vbmeta {
				/*we need use FirstStageMountVBootV2 if we enable avb*/
				compatible = "android,vbmeta";
				/*parts means the partition witch can be mount in first stage*/
				parts = "vbmeta,boot,system,vendor";
			};
		};
	};
};

/ {
	model = "Engicam i.MX8MM OFCAP 2.0 board";
	compatible = "fsl,imx8mm-evk", "fsl,imx8mm";

	chosen {
		bootargs = "console=ttymxc1,115200 earlycon=ec_imx6q,0x30890000,115200";
		stdout-path = &uart2;
	};

	leds {
		compatible = "gpio-leds";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_gpio_led>;

		status {
			label = "status";
			gpios = <&gpio3 16 0>;
			default-state = "on";
		};
	};

	gpio-restart {
        compatible = "gpio-restart";
        gpios = <&gpio1 2 1>;
        priority = <128>;
        active-delay = <100>;
        inactive-delay = <100>;
        wait-delay = <3000>;
    };	

    backlight_lvds: backlight_lvds {
            compatible = "pwm-backlight";
            pinctrl-names = "default";
            pinctrl-0 = <&pinctrl_pwm1>;
            pwms = <&pwm1 0 1000000>;
			brightness-levels = < 0  1  2  3  4  5  6  7  8  9
								 10 11 12 13 14 15 16 17 18 19
								 20 21 22 23 24 25 26 27 28 29
								 30 31 32 33 34 35 36 37 38 39
								 40 41 42 43 44 45 46 47 48 49
								 50 51 52 53 54 55 56 57 58 59
								 60 61 62 63 64 65 66 67 68 69
								 70 71 72 73 74 75 76 77 78 79
								 80 81 82 83 84 85 86 87 88 89
								 90 91 92 93 94 95 96 97 98 99
								 100>;
			default-brightness-level = <100>;
            status = "okay";
    };	

	bt_rfkill {
		compatible = "fsl,mxc_bt_rfkill";
		bt-power-gpios = <&gpio2 20 GPIO_ACTIVE_LOW>;
		status ="okay";
	};

	ir_recv: ir-receiver {
		compatible = "gpio-ir-receiver";
		gpios = <&gpio1 13 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_ir_recv>;
	};

    sound {
        compatible = "simple-audio-card";
        simple-audio-card,name = "sgtl5000-audio";
        simple-audio-card,format = "i2s";
        simple-audio-card,bitclock-master = <&dailink_master>;
        simple-audio-card,frame-master = <&dailink_master>;
        /*simple-audio-card,mclk-fs = <1>;*/
        simple-audio-card,cpu {
                sound-dai = <&sai3>;
        };

        dailink_master: simple-audio-card,codec {
            sound-dai = <&sgtl5000>;
            clocks = <&clk IMX8MM_CLK_SAI3>;
        };   
    };

	regulators {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <0>;

		reg_sd1_vmmc: sd1_regulator {
			compatible = "regulator-fixed";
			regulator-name = "WLAN_EN";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			gpio = <&gpio2 10 GPIO_ACTIVE_HIGH>;
			off-on-delay = <20000>;
			startup-delay-us = <100>;
			enable-active-high;
		};

		reg_usdhc2_vmmc: regulator-usdhc2 {
			compatible = "regulator-fixed";
			regulator-name = "VSD_3V3";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			gpio = <&gpio2 19 GPIO_ACTIVE_HIGH>;
			off-on-delay = <20000>;
			enable-active-high;
		};

        wlreg_on: fixedregulator@100 {
           compatible = "regulator-fixed";
           regulator-name = "wlreg_on";
           regulator-min-microvolt = <3300000>;
           regulator-max-microvolt = <3300000>;
           gpio = <&gpio1 4 GPIO_ACTIVE_HIGH>;
           enable-active-high;
        };

		reg_3v3_avdd_sgtl: reg_3v3_avdd_regulator {
			compatible = "regulator-fixed";
			regulator-name = "3v3_avdd_sgtl";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			always-on;
		};

		reg_3v3_sgtl: reg_3v3_sgtl_regulator {
			compatible = "regulator-fixed";
			regulator-name = "3v3_sgtl";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			always-on;
		};

		reg_1v8_sgtl: reg_1v8_sgtl_regulator {
			compatible = "regulator-fixed";
			regulator-name = "1v8_sgtl";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			always-on;
		};		
	};
};

&pwm1{
    status = "okay";
};

&clk {
	assigned-clocks = <&clk IMX8MM_AUDIO_PLL1>, <&clk IMX8MM_AUDIO_PLL2>;
	assigned-clock-rates = <393216000>, <361267200>;
};

&iomuxc {
	pinctrl-names = "default";

	imx8mm-evk {
        pinctrl_laird: lairdgrp {
            fsl,pins = <
                MX8MM_IOMUXC_GPIO1_IO04_GPIO1_IO4	0x19
				MX8MM_IOMUXC_SD2_WP_GPIO2_IO20		0x19
            >;
        };

        pinctrl_dsi_lvds_bridge: lvds_bridge_gpio {
            fsl,pins = <
                MX8MM_IOMUXC_NAND_DATA03_GPIO3_IO9	0x19
				MX8MM_IOMUXC_NAND_DATA02_GPIO3_IO8	0x19
            >;
        };

        pinctrl_pwm1: pwm1grp {
            fsl,pins = <
                 MX8MM_IOMUXC_SPDIF_EXT_CLK_PWM1_OUT     0x19
            >;
        };

		pinctrl_csi_pwn: csi_pwn_grp {
			fsl,pins = <
				MX8MM_IOMUXC_SAI2_RXC_GPIO4_IO22		0x19
			>;
		};

		pinctrl_ir_recv: ir_recv {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO13_GPIO1_IO13		0x14f
			>;
		};

		pinctrl_csi_rst: csi_rst_grp {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO12_GPIO1_IO12		0x19
			>;
		};

		pinctrl_mipi_dsi_en: mipi_dsi_en {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO08_GPIO1_IO8		0x16
			>;
		};

		pinctrl_i2c2_synaptics_dsx_io: synaptics_dsx_iogrp {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO09_GPIO1_IO9		0x19	/* Touch int */
			>;
		};

		pinctrl_fec1: fec1grp {
			fsl,pins = <
				MX8MM_IOMUXC_ENET_MDC_ENET1_MDC		0x3
				MX8MM_IOMUXC_ENET_MDIO_ENET1_MDIO	0x3
				MX8MM_IOMUXC_ENET_TD3_ENET1_RGMII_TD3	0x1f
				MX8MM_IOMUXC_ENET_TD2_ENET1_RGMII_TD2	0x1f
				MX8MM_IOMUXC_ENET_TD1_ENET1_RGMII_TD1	0x1f
				MX8MM_IOMUXC_ENET_TD0_ENET1_RGMII_TD0	0x1f
				MX8MM_IOMUXC_ENET_RD3_ENET1_RGMII_RD3	0x91
				MX8MM_IOMUXC_ENET_RD2_ENET1_RGMII_RD2	0x91
				MX8MM_IOMUXC_ENET_RD1_ENET1_RGMII_RD1	0x91
				MX8MM_IOMUXC_ENET_RD0_ENET1_RGMII_RD0	0x91
				MX8MM_IOMUXC_ENET_TXC_ENET1_RGMII_TXC	0x1f
				MX8MM_IOMUXC_ENET_RXC_ENET1_RGMII_RXC	0x91
				MX8MM_IOMUXC_ENET_RX_CTL_ENET1_RGMII_RX_CTL	0x91
				MX8MM_IOMUXC_ENET_TX_CTL_ENET1_RGMII_TX_CTL	0x1f
				MX8MM_IOMUXC_NAND_DATA01_GPIO3_IO7	0x19
			>;
		};

		pinctrl_flexspi0: flexspi0grp {
			fsl,pins = <
				MX8MM_IOMUXC_NAND_ALE_QSPI_A_SCLK		0x1c2
				MX8MM_IOMUXC_NAND_CE0_B_QSPI_A_SS0_B		0x82
				MX8MM_IOMUXC_NAND_DATA00_QSPI_A_DATA0		0x82
				MX8MM_IOMUXC_NAND_DATA01_QSPI_A_DATA1		0x82
				MX8MM_IOMUXC_NAND_DATA02_QSPI_A_DATA2		0x82
				MX8MM_IOMUXC_NAND_DATA03_QSPI_A_DATA3		0x82
			>;
		};

		pinctrl_gpio_led: gpioledgrp {
			fsl,pins = <
				MX8MM_IOMUXC_NAND_READY_B_GPIO3_IO16	0x19
			>;
		};

		pinctrl_i2c1: i2c1grp {
			fsl,pins = <
				MX8MM_IOMUXC_I2C1_SCL_I2C1_SCL			0x400001c3
				MX8MM_IOMUXC_I2C1_SDA_I2C1_SDA			0x400001c3
			>;
		};

		pinctrl_i2c2: i2c2grp {
			fsl,pins = <
				MX8MM_IOMUXC_I2C2_SCL_I2C2_SCL			0x400001c3
				MX8MM_IOMUXC_I2C2_SDA_I2C2_SDA			0x400001c3
			>;
		};

		pinctrl_i2c3: i2c3grp {
			fsl,pins = <
				MX8MM_IOMUXC_I2C3_SCL_I2C3_SCL			0x400001c3
				MX8MM_IOMUXC_I2C3_SDA_I2C3_SDA			0x400001c3
			>;
		};

		pinctrl_i2c4: i2c4grp {
			fsl,pins = <
				MX8MM_IOMUXC_I2C4_SCL_I2C4_SCL			0x400001c3
				MX8MM_IOMUXC_I2C4_SDA_I2C4_SDA			0x400001c3
			>;
		};

		pinctrl_pcie0: pcie0grp {
			fsl,pins = <
				MX8MM_IOMUXC_I2C4_SCL_PCIE1_CLKREQ_B	0x61 /* open drain, pull up */
				MX8MM_IOMUXC_GPIO1_IO05_GPIO1_IO5	0x41
				MX8MM_IOMUXC_SAI2_RXFS_GPIO4_IO21	0x41
			>;
		};

		pinctrl_pmic: pmicirq {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO03_GPIO1_IO3		0x41
			>;
		};

		pinctrl_typec1: typec1grp {
			fsl,pins = <
				MX8MM_IOMUXC_SD1_STROBE_GPIO2_IO11	0x159
			>;
		};

		pinctrl_typec2: typec2grp {
			fsl,pins = <
				MX8MM_IOMUXC_SD2_CD_B_GPIO2_IO12	0x159
			>;
		};

		pinctrl_sai1: sai1grp {
			fsl,pins = <
				MX8MM_IOMUXC_SAI1_MCLK_SAI1_MCLK	0xd6
				MX8MM_IOMUXC_SAI1_TXFS_SAI1_TX_SYNC	0xd6
				MX8MM_IOMUXC_SAI1_RXD7_SAI1_TX_SYNC	0xd6
				MX8MM_IOMUXC_SAI1_TXC_SAI1_TX_BCLK	0xd6
				MX8MM_IOMUXC_SAI1_TXD0_SAI1_TX_DATA0	0xd6
				MX8MM_IOMUXC_SAI1_TXD1_SAI1_TX_DATA1	0xd6
				MX8MM_IOMUXC_SAI1_TXD2_SAI1_TX_DATA2	0xd6
				MX8MM_IOMUXC_SAI1_TXD3_SAI1_TX_DATA3	0xd6
				MX8MM_IOMUXC_SAI1_TXD4_SAI1_TX_DATA4	0xd6
				MX8MM_IOMUXC_SAI1_TXD5_SAI1_TX_DATA5	0xd6
				MX8MM_IOMUXC_SAI1_TXD6_SAI1_TX_DATA6	0xd6
				MX8MM_IOMUXC_SAI1_TXD7_SAI1_TX_DATA7	0xd6
			>;
		};

		pinctrl_sai1_dsd: sai1grp_dsd {
			fsl,pins = <
				MX8MM_IOMUXC_SAI1_MCLK_SAI1_MCLK	0xd6
				MX8MM_IOMUXC_SAI1_TXFS_SAI1_TX_SYNC	0xd6
				MX8MM_IOMUXC_SAI1_RXD7_SAI1_TX_DATA4	0xd6
				MX8MM_IOMUXC_SAI1_TXC_SAI1_TX_BCLK	0xd6
				MX8MM_IOMUXC_SAI1_TXD0_SAI1_TX_DATA0	0xd6
				MX8MM_IOMUXC_SAI1_TXD1_SAI1_TX_DATA1	0xd6
				MX8MM_IOMUXC_SAI1_TXD2_SAI1_TX_DATA2	0xd6
				MX8MM_IOMUXC_SAI1_TXD3_SAI1_TX_DATA3	0xd6
				MX8MM_IOMUXC_SAI1_TXD4_SAI1_TX_DATA4	0xd6
				MX8MM_IOMUXC_SAI1_TXD5_SAI1_TX_DATA5	0xd6
				MX8MM_IOMUXC_SAI1_TXD6_SAI1_TX_DATA6	0xd6
				MX8MM_IOMUXC_SAI1_TXD7_SAI1_TX_DATA7	0xd6
			>;
		};

		pinctrl_sai3: sai3grp {
			fsl,pins = <
				MX8MM_IOMUXC_SAI3_TXFS_SAI3_TX_SYNC     0xd6
				MX8MM_IOMUXC_SAI3_TXC_SAI3_TX_BCLK      0xd6
				MX8MM_IOMUXC_SAI3_MCLK_SAI3_MCLK        0xd6
				MX8MM_IOMUXC_SAI3_TXD_SAI3_TX_DATA0     0xd6
				MX8MM_IOMUXC_SAI3_RXD_SAI3_RX_DATA0		0xd6
			>;
		};

		pinctrl_sai5: sai5grp {
			fsl,pins = <
				MX8MM_IOMUXC_SAI5_MCLK_SAI5_MCLK	0xd6
				MX8MM_IOMUXC_SAI5_RXC_SAI5_RX_BCLK	0xd6
				MX8MM_IOMUXC_SAI5_RXFS_SAI5_RX_SYNC	0xd6
				MX8MM_IOMUXC_SAI5_RXD0_SAI5_RX_DATA0	0xd6
				MX8MM_IOMUXC_SAI5_RXD1_SAI5_RX_DATA1    0xd6
				MX8MM_IOMUXC_SAI5_RXD2_SAI5_RX_DATA2    0xd6
				MX8MM_IOMUXC_SAI5_RXD3_SAI5_RX_DATA3    0xd6
			>;
		};

		pinctrl_pdm: pdmgrp {
			fsl,pins = <
				MX8MM_IOMUXC_SAI5_MCLK_SAI5_MCLK	0xd6
				MX8MM_IOMUXC_SAI5_RXC_PDM_CLK		0xd6
				MX8MM_IOMUXC_SAI5_RXFS_SAI5_RX_SYNC	0xd6
				MX8MM_IOMUXC_SAI5_RXD0_PDM_DATA0	0xd6
				MX8MM_IOMUXC_SAI5_RXD1_PDM_DATA1	0xd6
				MX8MM_IOMUXC_SAI5_RXD2_PDM_DATA2	0xd6
				MX8MM_IOMUXC_SAI5_RXD3_PDM_DATA3	0xd6
			>;
		};

		pinctrl_spdif1: spdif1grp {
			fsl,pins = <
				MX8MM_IOMUXC_SPDIF_TX_SPDIF1_OUT	0xd6
				MX8MM_IOMUXC_SPDIF_RX_SPDIF1_IN		0xd6
			>;
		};

		pinctrl_uart1: uart1grp {
			fsl,pins = <
				MX8MM_IOMUXC_UART1_RXD_UART1_DCE_RX	0x140
				MX8MM_IOMUXC_UART1_TXD_UART1_DCE_TX	0x140
			>;
		};

		pinctrl_uart2: uart2grp {
			fsl,pins = <
				MX8MM_IOMUXC_UART2_RXD_UART2_DCE_RX	0x140
				MX8MM_IOMUXC_UART2_TXD_UART2_DCE_TX	0x140
			>;
		};

		pinctrl_uart3: uart3grp {
			fsl,pins = <
				MX8MM_IOMUXC_UART3_TXD_UART3_DCE_TX			0x140
				MX8MM_IOMUXC_UART3_RXD_UART3_DCE_RX			0x140
				MX8MM_IOMUXC_ECSPI1_SS0_UART3_DCE_RTS_B		0x140
				MX8MM_IOMUXC_ECSPI1_MISO_UART3_DCE_CTS_B	0x140
			>;
		};

		pinctrl_usdhc1_gpio: usdhc1grpgpio {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO06_GPIO1_IO6	0x140
			>;
		};

		pinctrl_usdhc1: usdhc1grp {
			fsl,pins = <
				MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK		0x190
				MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD		0x1d0
				MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0	0x1d0
				MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1	0x1d0
				MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2	0x1d0
				MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3	0x1d0
			>;
		};

		pinctrl_usdhc1_100mhz: usdhc1grp100mhz {
			fsl,pins = <
				MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK		0x194
				MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD		0x1d4
				MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0	0x1d4
				MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1	0x1d4
				MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2	0x1d4
				MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3	0x1d4
			>;
		};

		pinctrl_usdhc1_200mhz: usdhc1grp200mhz {
			fsl,pins = <
				MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK		0x196
				MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD		0x1d6
				MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0	0x1d6
				MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1	0x1d6
				MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2	0x1d6
				MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3	0x1d6
			>;
		};

		pinctrl_usdhc2_gpio: usdhc2grpgpio {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO15_GPIO1_IO15	0x1c4
				MX8MM_IOMUXC_SD2_RESET_B_GPIO2_IO19	0x41
			>;
		};

		pinctrl_usdhc2: usdhc2grp {
			fsl,pins = <
				MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK		0x190
				MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD		0x1d0
				MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0	0x1d0
				MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1	0x1d0
				MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2	0x1d0
				MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3	0x1d0
			>;
		};

		pinctrl_usdhc2_100mhz: usdhc2grp100mhz {
			fsl,pins = <
				MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK		0x194
				MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD		0x1d4
				MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0	0x1d4
				MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1	0x1d4
				MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2	0x1d4
				MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3	0x1d4
			>;
		};

		pinctrl_usdhc2_200mhz: usdhc2grp200mhz {
			fsl,pins = <
				MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK		0x196
				MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD		0x1d6
				MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0	0x1d6
				MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1	0x1d6
				MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2	0x1d6
				MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3	0x1d6
			>;
		};

		pinctrl_usdhc3: usdhc3grp {
			fsl,pins = <
				MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x190
				MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d0
				MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x1d0
				MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x1d0
				MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x1d0
				MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x1d0
				MX8MM_IOMUXC_NAND_RE_B_USDHC3_DATA4		0x1d0
				MX8MM_IOMUXC_NAND_CE2_B_USDHC3_DATA5		0x1d0
				MX8MM_IOMUXC_NAND_CE3_B_USDHC3_DATA6		0x1d0
				MX8MM_IOMUXC_NAND_CLE_USDHC3_DATA7		0x1d0
				MX8MM_IOMUXC_NAND_CE1_B_USDHC3_STROBE 		0x190
			>;
		};

		pinctrl_usdhc3_100mhz: usdhc3grp100mhz {
			fsl,pins = <
				MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x194
				MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d4
				MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x1d4
				MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x1d4
				MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x1d4
				MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x1d4
				MX8MM_IOMUXC_NAND_RE_B_USDHC3_DATA4		0x1d4
				MX8MM_IOMUXC_NAND_CE2_B_USDHC3_DATA5		0x1d4
				MX8MM_IOMUXC_NAND_CE3_B_USDHC3_DATA6		0x1d4
				MX8MM_IOMUXC_NAND_CLE_USDHC3_DATA7		0x1d4
				MX8MM_IOMUXC_NAND_CE1_B_USDHC3_STROBE 		0x194
			>;
		};

		pinctrl_usdhc3_200mhz: usdhc3grp200mhz {
			fsl,pins = <
				MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x196
				MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d6
				MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x1d6
				MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x1d6
				MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x1d6
				MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x1d6
				MX8MM_IOMUXC_NAND_RE_B_USDHC3_DATA4		0x1d6
				MX8MM_IOMUXC_NAND_CE2_B_USDHC3_DATA5		0x1d6
				MX8MM_IOMUXC_NAND_CE3_B_USDHC3_DATA6		0x1d6
				MX8MM_IOMUXC_NAND_CLE_USDHC3_DATA7		0x1d6
				MX8MM_IOMUXC_NAND_CE1_B_USDHC3_STROBE 		0x196
			>;
		};
		edt_ft5x06_pins: edt-ft5x06-pins-grp {
            fsl,pins = <
				MX8MM_IOMUXC_SAI5_RXFS_GPIO3_IO19		0xc6
				MX8MM_IOMUXC_GPIO1_IO08_GPIO1_IO8		0x19
            >;	
		};

		pinctrl_wdog: wdoggrp {
			fsl,pins = <
				MX8MM_IOMUXC_GPIO1_IO02_GPIO1_IO2		0x19
			>;
		};
	};
};

&csi1_bridge {
	fsl,mipi-mode;
	status = "okay";
	port {
		csi1_ep: endpoint {
			remote-endpoint = <&csi1_mipi_ep>;
		};
	};
};

&flexspi {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_flexspi0>;
	status = "disabled";

	flash0: mt25qu256aba@0 {
		reg = <0>;
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "micron,mt25qu256aba";
		spi-max-frequency = <80000000>;
		spi-nor,ddr-quad-read-dummy = <6>;
	};
};

&i2c1 {
	clock-frequency = <400000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";

	pmic: bd71837@4b {
		reg = <0x4b>;
		compatible = "rohm,bd71840", "rohm,bd71837";
		/* PMIC BD71837 PMIC_nINT GPIO1_IO3 */
		pinctrl-0 = <&pinctrl_pmic>;
		gpio_intr = <&gpio1 3 GPIO_ACTIVE_LOW>;

		gpo {
			rohm,drv = <0x0C>;	/* 0b0000_1100 all gpos with cmos output mode */
		};

		regulators {
			#address-cells = <1>;
			#size-cells = <0>;

			bd71837,pmic-buck2-uses-i2c-dvs;
			bd71837,pmic-buck2-dvs-voltage = <1000000>, <900000>, <0>; /* VDD_ARM: Run-Idle */

			buck1_reg: regulator@0 {
				reg = <0>;
				regulator-compatible = "buck1";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1300000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <1250>;
			};

			buck2_reg: regulator@1 {
				reg = <1>;
				regulator-compatible = "buck2";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1300000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <1250>;
			};

			buck3_reg: regulator@2 {
				reg = <2>;
				regulator-compatible = "buck3";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1300000>;
			};

			buck4_reg: regulator@3 {
				reg = <3>;
				regulator-compatible = "buck4";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1300000>;
			};

			buck5_reg: regulator@4 {
				reg = <4>;
				regulator-compatible = "buck5";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1350000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck6_reg: regulator@5 {
				reg = <5>;
				regulator-compatible = "buck6";
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck7_reg: regulator@6 {
				reg = <6>;
				regulator-compatible = "buck7";
				regulator-min-microvolt = <1605000>;
				regulator-max-microvolt = <1995000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck8_reg: regulator@7 {
				reg = <7>;
				regulator-compatible = "buck8";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1400000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo1_reg: regulator@8 {
				reg = <8>;
				regulator-compatible = "ldo1";
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo2_reg: regulator@9 {
				reg = <9>;
				regulator-compatible = "ldo2";
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <900000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo3_reg: regulator@10 {
				reg = <10>;
				regulator-compatible = "ldo3";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo4_reg: regulator@11 {
				reg = <11>;
				regulator-compatible = "ldo4";
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo6_reg: regulator@13 {
				reg = <13>;
				regulator-compatible = "ldo6";
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};
		};
	};
};

&i2c2 {
	clock-frequency = <400000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c2>;
	status = "okay";

	adv_bridge: adv7535@3d {
			compatible = "adi,adv7533";
			reg = <0x3d>;
			adi,addr-cec = <0x3b>;
			adi,dsi-lanes = <4>;
			pinctrl-names = "default";
			pinctrl-0 = <&pinctrl_i2c2_synaptics_dsx_io>;
			interrupt-parent = <&gpio1>;
			interrupts = <9 IRQ_TYPE_LEVEL_LOW>;
			status = "disabled";
	};

	pcf8563: rtc@51 {
		compatible = "pcf8563";
		reg = <0x51>;
	};

	sgtl5000: codec@a {
		#sound-dai-cells = <0>;
		compatible = "fsl,sgtl5000";
		status = "okay";
		reg = <0x0a>;
		clocks = <&clk IMX8MM_CLK_SAI3>;
		clock-names = "mclk";
		assigned-clock-rates = <24576000>;                
		VDDA-supply = <&reg_3v3_avdd_sgtl>;
		VDDIO-supply = <&reg_3v3_sgtl>;
		VDDD-supply = <&reg_1v8_sgtl>;
	};

    polytouch: edt-ft5x26@38 {
                compatible = "edt,edt-ft5x26";
                reg = <0x38>;
                pinctrl-names = "default";
                pinctrl-0 = <&edt_ft5x06_pins>;
                interrupt-parent = <&gpio3>;
                interrupts = <19 IRQ_TYPE_EDGE_FALLING>;
                reset-gpios= <&gpio1 8 GPIO_ACTIVE_LOW>;
    };

	ov5640_mipi: ov5640_mipi@3c {
		compatible = "ovti,ov5640_mipi";
		reg = <0x3c>;
		status = "okay";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_csi_pwn>, <&pinctrl_csi_rst>;
		clocks = <&clk IMX8MM_CLK_CLKO1>;
		clock-names = "csi_mclk";
		assigned-clocks = <&clk IMX8MM_CLK_CLKO1>;
		assigned-clock-parents = <&clk IMX8MM_CLK_24M>;
		assigned-clock-rates = <24000000>;
		csi_id = <0>;
		pwn-gpios = <&gpio4 22 GPIO_ACTIVE_HIGH>;
		mclk = <24000000>;
		mclk_source = <0>;
		port {
			ov5640_mipi1_ep: endpoint {
				remote-endpoint = <&mipi1_sensor_ep>;
			};
		};
	};
};


&mipi_csi_1 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	port {
		mipi1_sensor_ep: endpoint1 {
			remote-endpoint = <&ov5640_mipi1_ep>;
			data-lanes = <2>;
			csis-hs-settle = <13>;
			csis-clk-settle = <2>;
			csis-wclk;
		};

		csi1_mipi_ep: endpoint2 {
			remote-endpoint = <&csi1_ep>;
		};
	};
};

&i2c3 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c3>;
	status = "okay";
};

&i2c4 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c4>;
	status = "okay";

    lvds_bridge: sn65dsi83@2c {
        compatible = "ti,sn65dsi83";
        reg = <0x2c>;
        ti,dsi-lanes = <2>;
        ti,lvds-format = <2>;
        ti,lvds-bpp = <24>;
        ti,width-mm = <149>;
        ti,height-mm = <93>;
		enable-gpios = <&gpio3 9  GPIO_ACTIVE_HIGH>;
        pinctrl-names = "default";
        pinctrl-0 = <&pinctrl_dsi_lvds_bridge>;
        status = "okay";

		display-timings {
			native-mode = <&timing1>;
			timing1: hsd100pxn1 {
				clock-frequency = <50000000>;
				hactive = <1024>;
				vactive = <600>;
				hback-porch = <150>;
				hfront-porch = <150>;
				vback-porch = <10>;
				vfront-porch = <10>;
				hsync-len = <20>;
				vsync-len = <15>;

				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <1>;
				pixelclk-active = <0>;
			};
        };

        port {
            sn65dsi83_in: endpoint {
                remote-endpoint = <&dsim_to_sn65dsi8>;
            };
        };
    };

};

&lcdif {
	status = "okay";
};

&mipi_dsi {
	status = "okay";

	port@1 {
		dsim_to_sn65dsi8: endpoint {
			remote-endpoint = <&sn65dsi83_in>;
		};
	};
};

&mu {
	status = "okay";
};

&sai1 {
	pinctrl-names = "default", "dsd";
	pinctrl-0 = <&pinctrl_sai1>;
	pinctrl-1 = <&pinctrl_sai1_dsd>;
	assigned-clocks = <&clk IMX8MM_CLK_SAI1>;
	assigned-clock-parents = <&clk IMX8MM_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <49152000>;
	clocks = <&clk IMX8MM_CLK_SAI1_IPG>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_SAI1_ROOT>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_DUMMY>, <&clk IMX8MM_AUDIO_PLL1_OUT>,
		<&clk IMX8MM_AUDIO_PLL2_OUT>;
	clock-names = "bus", "mclk0", "mclk1", "mclk2", "mclk3", "pll8k", "pll11k";
	fsl,sai-multi-lane;
	fsl,dataline,dsd = <0 0xff 0xff 2 0xff 0x11>;
	dmas = <&sdma2 0 26 0>, <&sdma2 1 26 0>;
	status = "disabled";
};

&sai3 {
	#sound-dai-cells = <0>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai3>;
	assigned-clocks = <&clk IMX8MM_CLK_SAI3>;
	assigned-clock-parents = <&clk IMX8MM_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <24576000>;
	status = "okay";
};

&sai5 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai5>;
	assigned-clocks = <&clk IMX8MM_CLK_SAI5>;
	assigned-clock-parents = <&clk IMX8MM_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <49152000>;
	clocks = <&clk IMX8MM_CLK_SAI5_IPG>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_SAI5_ROOT>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_DUMMY>, <&clk IMX8MM_AUDIO_PLL1_OUT>,
		<&clk IMX8MM_AUDIO_PLL2_OUT>;
	clock-names = "bus", "mclk0", "mclk1", "mclk2", "mclk3", "pll8k", "pll11k";
	fsl,sai-asynchronous;
	status = "disabled";
};

&spdif1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_spdif1>;
	assigned-clocks = <&clk IMX8MM_CLK_SPDIF1>;
	assigned-clock-parents = <&clk IMX8MM_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <24576000>;
	clocks = <&clk IMX8MM_CLK_AUDIO_AHB>, <&clk IMX8MM_CLK_24M>,
		<&clk IMX8MM_CLK_SPDIF1>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_DUMMY>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_AUDIO_AHB>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_CLK_DUMMY>, <&clk IMX8MM_CLK_DUMMY>,
		<&clk IMX8MM_AUDIO_PLL1_OUT>, <&clk IMX8MM_AUDIO_PLL2_OUT>;
	clock-names = "core", "rxtx0", "rxtx1", "rxtx2", "rxtx3",
		"rxtx4", "rxtx5", "rxtx6", "rxtx7", "spba", "pll8k", "pll11k";
	status = "disabled";
};

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_fec1>;
	phy-mode = "rgmii-id";
	phy-handle = <&ethphy0>;
	//fsl,magic-packet;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@0 {
			reg = <3>;
			compatible = "ethernet-phy-ieee802.3-c22";
            device_type = "ethernet-phy";
            rxc-skew-ps = <3000>;
            rxdv-skew-ps = <0>;
            txc-skew-ps = <3000>;
            txen-skew-ps = <0>;
		};
	};
};

&pcie0{
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie0>;
	disable-gpio = <&gpio1 5 GPIO_ACTIVE_LOW>;
	reset-gpio = <&gpio4 21 GPIO_ACTIVE_LOW>;
	ext_osc = <1>;
	status = "disabled";
};

&uart1 { /* RS232 */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	assigned-clocks = <&clk IMX8MM_CLK_UART1>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL1_80M>;
	status = "okay";
};

&uart2 { /* console */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	status = "okay";
};

&uart3 { /* RS485 */
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	assigned-clocks = <&clk IMX8MM_CLK_UART3>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL1_80M>;
	fsl,uart-has-rtscts;
	status = "okay";
};

&usbotg1 {
	dr_mode = "otg";
	//extcon = <0>, <&typec1_ptn5110>;
	picophy,pre-emp-curr-control = <3>;
	picophy,dc-vol-level-adjust = <7>;
	status = "okay";
};

&usbotg2 {
	dr_mode = "host";
	//extcon = <0>, <&typec2_ptn5110>;
	picophy,pre-emp-curr-control = <3>;
	picophy,dc-vol-level-adjust = <7>;
	status = "okay";
};

&usdhc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usdhc1>, <&pinctrl_usdhc1_gpio>;
	cd-gpios = <&gpio1 6 GPIO_ACTIVE_LOW>;
	max-frequency = <50000000>;
	bus-width = <4>;
	no-1-8-v;
	pm-ignore-notify;
	keep-power-in-suspend;	
	vmmc-supply = <&reg_sd1_vmmc>;
	status = "okay";
};

&usdhc2 {
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc2>, <&pinctrl_laird>;
	pinctrl-1 = <&pinctrl_usdhc2_100mhz>, <&pinctrl_laird>;
	pinctrl-2 = <&pinctrl_usdhc2_200mhz>, <&pinctrl_laird>;
	non-removable;
	bus-width = <4>;
    no-1-8-v;
    vmmc-supply = <&wlreg_on>;
	status = "okay";
};

&usdhc3 {
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc3>;
	pinctrl-1 = <&pinctrl_usdhc3_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc3_200mhz>;
	bus-width = <8>;
	non-removable;
	status = "okay";
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,ext-reset-output;
	status = "okay";
};

&A53_0 {
	arm-supply = <&buck2_reg>;
};

&gpu {
	status = "okay";
};

&vpu_g1 {
	status = "okay";
};

&vpu_g2 {
	status = "okay";
};

&vpu_h1 {
	status = "okay";
};

&crypto {
       status = "disabled";
};
